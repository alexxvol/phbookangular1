'use strict';

// Register `phoneList` component, along with its associated controller and template
angular.
module('phoneBook').
component('phoneBook', {
    templateUrl: 'phone-book/phone-book.template.html',
    controller: function PhoneListController($http) {
        this.mock = 'mock/abons.json';
        this.server = 'http://589b1131bc99bf120037b98c.mockapi.io/api/v1/phones';
        this.label = "Телефонный справочник";
        //создадим временные переменные (запихнул в объект)
        this.objectPhones = {
            inpId: "",
            inpName: "",
            inpSecondName: "",
            inpPhone: "",
            inpDate: ""
        }

        //http:

        //get
        var self = this;
        $http.get(this.server).then(function(response) {
            self.abons = response.data;
        });

        //post
        this.add = function() {
            if (this.objectPhones.inpName && this.objectPhones.inpSecondName && this.objectPhones.inpPhone && this.objectPhones.inpDate) {
                $http.post(this.server, {
                    firstName: this.objectPhones.inpName,
                    secondName: this.objectPhones.inpSecondName,
                    phone: this.objectPhones.inpPhone,
                    createdAt: this.objectPhones.inpDate
                }).then(function(response) {
                    console.log(response);
                });
            } else {
                console.log("Заполните поля")
            }
        };

        //delete


        // this.addNewAbon = function() {
        //     //созд новый объект
        //     var asd = {
        //         id: this.objectPhones.inpId,
        //         firstName: this.objectPhones.inpName,
        //         secondName: this.objectPhones.inpSecondName,
        //         phone: this.objectPhones.inpPhone,
        //         createdAt: this.objectPhones.inpDate,
        //     }
        //     //пушим asd в abons c проверкой полей
        //     if (this.objectPhones.inpId && this.objectPhones.inpName && this.objectPhones.inpSecondName && this.objectPhones.inpPhone && this.objectPhones.inpDate) {
        //         this.abons.push(asd);
        //     } else {
        //         console.log("Заполните поля")
        //     }
        // };

        this.delete = function(item) {
            var index = this.abons.indexOf(item);
            console.log(index);
            this.abons.splice(index, 1);

        }
    }
});