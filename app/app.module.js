'use strict';

angular.module('phonebookApp', [
    'ngRoute',
    'phoneBook',
    'phonebookDetail'
]);