angular.
module('phonebookApp').
config(['$locationProvider', '$routeProvider',
    function config($locationProvider, $routeProvider) {
        $locationProvider.hashPrefix('!');

        $routeProvider.
        when('/abons', {
            template: '<phone-book></phone-book>'
        }).
        when('/abons/:abonsId', {
            template: '<phonebook-detail></phonebook-detail>'
        }).
        otherwise('/abons');
    }
]);